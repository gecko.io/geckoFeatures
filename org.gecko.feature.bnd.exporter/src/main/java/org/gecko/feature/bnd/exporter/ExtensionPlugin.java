package org.gecko.feature.bnd.exporter;

import java.util.Map;

import org.apache.sling.feature.ExtensionState;
import org.apache.sling.feature.ExtensionType;

import aQute.bnd.annotation.plugin.BndPlugin;
import aQute.bnd.service.Plugin;
import aQute.service.reporter.Reporter;

@BndPlugin(name = "FeatureExtensionPlugin")
public class ExtensionPlugin implements Plugin {

    private Map<String, String> map;

    @Override
    public void setProperties(Map<String, String> map) throws Exception {

        this.map = map;

    }

    @Override
    public void setReporter(Reporter processor) {

    }

    public org.apache.sling.feature.Extension toExtension() {

        String sType = map.getOrDefault("type", ExtensionType.ARTIFACTS.toString());
        String sState = map.getOrDefault("state", ExtensionState.OPTIONAL.toString());
        String sName = map.getOrDefault("name", "unknmown");
        String sText = map.getOrDefault("text", "");

        ExtensionType type = ExtensionType.valueOf(sType);
        ExtensionState state = ExtensionState.valueOf(sState);
        org.apache.sling.feature.Extension e = new org.apache.sling.feature.Extension(type, sName,
                state);

        if (ExtensionType.JSON.equals(type)) {
            e.setJSON(sText);
        } else if (ExtensionType.TEXT.equals(type)) {
            e.setText(sText);
        }

        return e;
    }

}
