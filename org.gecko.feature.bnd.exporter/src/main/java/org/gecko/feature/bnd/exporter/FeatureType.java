package org.gecko.feature.bnd.exporter;

public enum FeatureType {
    JSON,
    CONFIG_JSON,
	JAR,
	ARCHIVE;
}
